# README #

### MitRejsekort ###

MitRejsekort blev oprindeligt udviklet af ??? men da han ikke havde ressourcer til at vedligeholde tre apps på tre platforme, stoppede udviklingen på app'en
til Windows Phone (Windows 10 Mobile).

Hans Facebook-gruppe findes (stadig) på [MitRejsekort](https://www.facebook.com/mitrejsekort) .

Heldigvis har han valgt at frigive kildekoden under en OSS-licens, så vi andre kan arbejde videre med den - tak for det!


### Hvordan bidrager jeg? ###

Fork away :-)

### Hvem skal jeg snakke med? ###

Vedligeholder af dette repository: Jesper Lund Stocholm

### Licens ###

Ophavsret til kildekoden er stadig ??? og den er licenseret under Open Source licensen [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)